Connect-ExchangeOnline
$User = Read-Host -Prompt 'Please enter the E-Mail address of the user'
$DistributionList=@(
"Accounting"
"Managers"
"WholeCompany"
)
ForEach($Item In $DistributionList){
Remove-DistributionGroupMember -Identity $Item -Member $User
}
