﻿$subnets = Import-Csv ‘C:\Scripts\subnets.csv‘


# Loop through the CSV
    foreach ($subnet in $subnets) {

    $subnetProps = @{

      Name          = $subnet.Name
      Site          = $subnet.Site
      Location      = $subnet.Location
      Description   = $subnet.Description

      }#end groupProps

    New-ADReplicationSubnet -Server yourdc.yourdomain.ext @subnetProps
    Write-Host "Created subnet"$subnet.Name"linked to the"$subnet.Site"site in"$subnet.Location"location."
    
} #end foreach loop