Try { Import-Module ActiveDirectory -ErrorAction Stop }
Catch { Write-Host "Unable to load Active Directory module, is RSAT installed?"; Break }
$OUList=@(
"Accounting"
"Accounts"
"Customer Service"
"Development"
"Executive"
"HR"
"Marketing"
"Operations"
"SysAdmin"
"TechSupport"
)
Set-Content "C:\Scripts\Users-Exported-$(get-date -f 'yyyy-MM-dd').csv" -Value "CN,Title,UserPrincipalName,Office,Department,Manager,MemberOf,Country"
$PathTotal = "C:\Scripts\Users-Exported-$(get-date -f 'yyyy-MM-dd').csv"
ForEach($Item In $OUList){
$OU = "OU=$Item,OU=Employees,DC=Domain,DC=Extension"
Get-ADUser -SearchBase $OU -Filter {Enabled -eq $true} -Properties CN,Title,UserPrincipalName,Office,Department,Manager,CanonicalName,co | select CN,Title,UserPrincipalName,Office,Department,@{label='Manager';expression={$_.manager -replace '^CN=|,.*$'}},@{label='MemberOf';expression={$Item}},co | Export-Csv Users-$Item-$(get-date -f 'yyyy-MM-dd').csv -notype
$PathOU = "C:\Scripts\Users-$Item-$(get-date -f 'yyyy-MM-dd').csv"
(Get-Content -Path $PathOU | select -Skip 1) | Add-Content -Path $PathTotal
}
