$OffboardedUser = Read-Host -Prompt 'Please enter username (first.last@company.com)'
Get-ADUser -SearchBase "OU=Employees,DC=us,DC=ad,DC=company,DC=com" -Filter "userPrincipalName -like '$OffboardedUser'" | % {Set-ADUser $_ -Country $null}
Get-ADUser -SearchBase "OU=Employees,DC=us,DC=ad,DC=company,DC=com" -Filter "userPrincipalName -like '$OffboardedUser'" | Disable-ADAccount -Confirm:$false
Get-ADUser -SearchBase "OU=Employees,DC=us,DC=ad,DC=company,DC=com" -Filter "userPrincipalName -like '$OffboardedUser'" | Move-AdObject -TargetPath "OU=Offboarded,OU=Employees,DC=us,DC=ad,DC=company,DC=com" -Confirm:$false
$OffUserAddresses = $OUAddress.ProxyAddresses
$OffUserName = $OUAddress.SAMAccountName
Set-ADUser $OffUserName -remove @{ ProxyAddresses = $OffUserAddresses } -Debug -Verbose
Connect-AzureAD
Get-AzureADUserMembership -ObjectID $OffboardedUser | Select DisplayName
