# Load functions in https://github.com/crossan007/PowerShell-Modules/blob/master/Dyn-DNS.psm1

$Token = Get-DYNLoginToken -CustomerName "<YourCompany>" -UserName "<YourUsername>" -Password "<YourPassword>"

Get-AllZones -Token $Token | Select -ExpandProperty ZoneName > Zones.txt

$ZonesFile = Get-Content Zones.txt
ForEach($Item in $ZonesFile){
Get-AllZoneRecords $Item -Token $Token | Select -ExpandProperty RecordURL > $Item-ZonesExpanded.txt
$ZonesExpandedFile = Get-Content $Item-ZonesExpanded.txt
ForEach($i in $ZonesExpandedFile){
Get-RecordDetails $i -Token $Token >> $Item-ZonesExpandedDetailed.txt
}
}