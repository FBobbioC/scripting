#Based on https://techcommunity.microsoft.com/t5/azure-active-directory-identity/bulk-update-with-company-name-in-azure-ad/m-p/1561135

#Connect-AzureAD

Function Get-FileName($InitialDirectory)
{
    [System.Reflection.Assembly]::LoadWithPartialName("System.windows.forms") | Out-Null

  $OpenFileDialog = New-Object System.Windows.Forms.OpenFileDialog
  $OpenFileDialog.initialDirectory = $initialDirectory
  $OpenFileDialog.filter = "CSV (*.csv) | *.csv"
  $OpenFileDialog.ShowDialog() | Out-Null
  $OpenFileDialog.FileName
}

$Path = Get-FileName

$CSVrecords = Import-Csv $Path -Delimiter ","

# Create arrays for skipped and failed users
$SkippedUsers = @()
$FailedUsers = @()

# Loop trough CSV records
ForEach ($CSVrecord in $CSVrecords) {
#$UserPN = $CSVRecord.PrincipalName
#$UserCountry = $CSVRecord.Country
#$UserPN
#$UserCountry
Set-AzureADUser -ObjectId $CSVRecord.PrincipalName -Country $CSVRecord.Country
}
# Array skipped users
# $SkippedUsers
# Array failed users
# $FailedUsers
