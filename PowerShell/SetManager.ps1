#Open script with Notepad++
#Based on https://4sysops.com/archives/configuring-ad-users-and-managers-with-powershell/
$UserManager = Read-Host -Prompt 'Please enter manager's username (first.last)'
$SetManagerList=@(
#Select the whole list of users, remove it, copy the E-Mail addresses from Excel and paste them below
#Select the list of E-Mail addresses, click on "Ctrl" + "H"
#Under "Find what:" enter "@company.com" (without the quotation marks)
#Under "Replace with" enter double quotation marks
#Mark the "In selection" checkbox
#Select "Regular expression" from the lower left
#Click on "Replace All"
#Under "Find what:" enter "^" (without the quotation marks) to enter the double quotations at the beginning of the line
#Note:  Use "$" (without the quotation marks) to enter the double quotations at the end of the line
#Under "Replace with" enter double quotation marks
#Mark the "In selection" checkbox
#Select "Regular expression" from the lower left
#Click on "Replace All"
#Click on "Close"
"User.1"
"User.2"
"User.3"
"User.4"
"User.5"
"User.6"
"User.7"
"User.8"
"User.9"
)
ForEach($Item In $SetManagerList){
Get-ADUser -Identity $Item | Set-ADUser -Manager $UserManager -PassThru | Get-ADUser -Properties Manager | Select Name,Manager
}
#Click on "File" -- "Save" of the blue diskette icon
