﻿$DHCPScopes = Import-Csv ‘C:\Scripts\DHCPScopes.csv‘


# Loop through the CSV
    foreach ($DHCPScope in $DHCPScopes) {

    $DHCPScopeProps = @{

      Name          = $DHCPScope.Name
      StartRange    = $DHCPScope.StartRange
      EndRange      = $DHCPScope.EndRange
	  SubnetMask    = $DHCPScope.SubnetMask
      Description   = $DHCPScope.Description

      }#end DHCPScopeProps

    Add-DhcpServerv4Scope -ComputerName yourdc.yourdomain.ext @DHCPScopeProps -State Active -LeaseDuration "1.00:00:00"
    Write-Host "Created DHCP scope"$DHCPScope.Name"starting with"$DHCPScope.StartRange"and ending in"$DHCPScope.EndRange""
    
} #end foreach loop

$DHCPOptionValues = Import-Csv ‘C:\Scripts\DHCPScopes.csv‘

foreach ($DHCPOptionValue in $DHCPOptionValues) {

    $DHCPOptionValuesProps = @{

	  Router        = $DHCPOptionValue.Router
	  ScopeId       = $DHCPOptionValue.ScopeId
	  DNSDomain     = $DHCPOptionValue.DNSDomain

      }#end DHCPOptionValues

	Set-DhcpServerv4OptionValue -ComputerName yourdc.yourdomain.ext @DHCPOptionValuesProps
    Write-Host "Setup router IP address "$DHCPOptionValue.Router"for DHCP scope"$DHCPOptionValue.ScopeId"with DNS domain"$DHCPOptionValue.DNSDomain""
	
} #end foreach loop

$DHCPDNSServers = Import-Csv ‘C:\Scripts\DHCPScopes.csv‘

foreach ($DHCPDNSServer in $DHCPDNSServers) {

    $DHCPDNSServersProps = @{

	  ScopeId       = $DHCPDNSServer.ScopeId
	  DNSServer     = $DHCPDNSServer.DNSServer

      }#end DHCPServers

	Set-DhcpServerv4OptionValue -ComputerName yourdc.yourdomain.ext -ScopeId $_.ScopeId -DNSServer $_.DnsServer.split('|')
    Write-Host "Setup DNS servers"$DHCPDNSServer.DNSServer"for DHCP scope"$DHCPDNSServer.ScopeId""
	
} #end foreach loop