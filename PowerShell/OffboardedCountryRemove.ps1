Get-ADUser -Filter 'Name -like "*"' -SearchBase 'OU=Offboarded,OU=Employees,DC=us,DC=ad,DC=company,DC=com' -Properties DisplayName | % {Set-ADUser $_ -Country $null}
