# Based on https://help.dyn.com/rest-resources/
#!/bin/bash
Token=$(curl -sX POST -d @Credentials.txt -H "Content-Type: application/json" https://api.dynect.net/REST/Session/ | awk -F '"' '{print $10}')
curl -sX GET -H "Content-Type: application/json" -H "Auth-Token: $Token" https://api.dynect.net/REST/Zone/ >> Zones.json
cat Zones.json | awk -F ']' '{print $1}' | awk -F '[' '{print $2}' > Zones.json.1
sed -i -e 's|/||g' -i -e 's|RESTZone||g' -i -e 's|,||g' -i -e 's|"||g' Zones.json.1
rm Zones.json && mv Zones.json.1 Zones.json
AllZones=$(cat Zones.json)
for val in ${AllZones[@]};do
  curl -sX GET -H "Content-Type: application/json" -H "Auth-Token: $Token" https://api.dynect.net/REST/AllRecord/$val/ >> $val.json
  echo -e Exporting data for zone $val
  cat $val.json | awk -F ']' '{print $1}' | awk -F '[' '{print $2}' > $val.json.1
  sed -i -e 's|,||g' -i -e 's|"||g' $val.json.1
  rm $val.json && mv $val.json.1 $val.json
done
for i in $(cat Zones.json);do
  EachZone=$(cat $i.json)
  for val in ${EachZone[@]};do
    EachZoneName=$(echo $val | awk -F '/' '{print $4}')
    ZoneNameAttribute=$(echo $val | awk -F '/' '{print $3}')
    curl -sX GET -H "Content-Type: application/json" -H "Auth-Token: $Token" https://api.dynect.net$val/ >> $EachZoneName-Detailed.json && echo >> $EachZoneName-Detailed.json && echo >> $EachZoneName-Detailed.json
    echo -e Exporting detailed data for $ZoneNameAttribute in zone $EachZoneName
    cat $i-Detailed.json | awk -F 'zone": ' '{print $2}' | awk -F ', "record_id":' '{print $1}' >> $i-Detailed-Cleaned.json
    sed -i -e 's|,|\n|g' -i -e 's|"||g' $i-Detailed-Cleaned.json
  done
done
#rm *-Detailed.json