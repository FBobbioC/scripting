#!/bin/bash
#Color Codes
Black='\033[0;30m'
Blue='\033[0;34m'
BrownOrange='\033[0;33m'
Cyan='\033[0;36m'
DarkGray='\033[1;30m'
Green='\033[0;32m'
LightBlue='\033[1;34m'
LightCyan='\033[1;36m'
LightGray='\033[0;37m'
LightGreen='\033[1;32m'
LightMagenta='\033[1;35m'
LightPurple='\033[1;35m'
LightRed='\033[1;31m'
LightYellow='\033[1;33m'
Magenta='\033[0;35m'
NC='\033[0m' # No Color
Purple='\033[0;35m'
Red='\033[0;31m'
White='\033[1;37m'
Yellow='\033[0;33m'

CurrentDate=$(date +"%Y-%m-%d %T")
echo -e ${Red}Date${NC} ${Green}Current Date + Time:${NC} $CurrentDate
InstalledKernel=$(uname -r)
LatestKernel=$(curl -s https://www.kernel.org/finger_banner | grep "latest stable version" | awk -F ":" 'NF >1 {print $2}' | awk '{gsub(/^[ \t]+| [ \t]+$/,""); print $0}')
UpdateKernelCheck=$(if [ $InstalledKernel == $LatestKernel ]; then echo -e ${LightBlue}OK${NC}; else echo -e ${LightRed}Update!!!${NC}; fi)
echo -e ${Red}Kernel${NC} ${Green}Installed:${NC} $InstalledKernel ${Green}Latest:${NC} $LatestKernel ${LightPurple}===${NC} $UpdateKernelCheck
InstalledSlackware64Current=$(find /var/log/packages/ -printf '%T+ %p\n' | sort -r | head -1 | awk -F "+" 'NF >1 {print $1}')
LatestSlackware64Current=$(curl -s https://ftp.osuosl.org/pub/slackware/slackware64-current/ | grep ChangeLog | awk -F ">" 'NF >1 {print $10}' | awk -F " " 'NF >1 {print $1}')
UpdateSlackware64CurrentCheck=$(if [ $InstalledSlackware64Current == $LatestSlackware64Current ]; then echo -e ${LightBlue}OK${NC}; else echo -e ${LightRed}Check!!!${NC}; fi)
echo -e ${Red}Slackware64-current${NC} ${Green}Installed:${NC} $InstalledSlackware64Current ${Green}Latest:${NC} $LatestSlackware64Current ${LightPurple}===${NC} $UpdateSlackware64CurrentCheck
echo ""
#Current1Password=$(1password --version | awk -F ".BETA" '{print $1}')
#Latest1Password=$(curl -s https://releases.1password.com/linux/beta/ | grep -m 1 "Updated to" | awk -F "Updated to " '{print $2}' | awk -F " on" '{print $1}')
#Update1PasswordCheck=$(if [ $Current1Password == $Latest1Password ]; then echo -e ${LightBlue}OK${NC}; else echo -e ${LightRed}Update!!!${NC}; fi)
#echo -e ${Red}1Password${NC} ${Green}Installed:${NC} $Current1Password ${Green}Latest:${NC} $Latest1Password ${LightPurple}===${NC} $Update1PasswordCheck
#CurrentAngryIPScanner=$(ls /var/log/packages | grep ipscan | awk -F "-" '{print $2}')
#LatestAngryIPScanner=$(git ls-remote --tags --refs --sort="version:refname" https://github.com/angryip/ipscan | awk -F/ 'END{print$NF}' | tr -d \v)
#UpdateAngryIPScannerCheck=$(if [ $CurrentAngryIPScanner == $LatestAngryIPScanner ]; then echo -e ${LightBlue}OK${NC}; else echo -e ${LightRed}Update!!!${NC}; fi)
#echo -e ${Red}AngryIPScanner${NC} ${Green}Installed:${NC} $CurrentAngryIPScanner ${Green}Latest:${NC} $LatestAngryIPScanner ${LightPurple}===${NC} $UpdateAngryIPScannerCheck
#CurrentApacheDirectoryStudio=$(ls -lah /Documents/Fernando/Downloads/TGZFiles/ApacheDirectoryStudio/ApacheDirectoryStudio*.tar.gz | awk -F ".v" '{print $2}' | awk -F "-linux" '{print $1}')
#LatestApacheDirectoryStudio=$(curl -s https://dlcdn.apache.org/directory/studio/ | grep .v | awk -F ".v" '{print $2}' | awk -F "/" '{print $1}')
#UpdateApacheDirectoryStudioCheck=$(if [ $CurrentApacheDirectoryStudio == $LatestApacheDirectoryStudio ]; then echo -e ${LightBlue}OK${NC}; else echo -e ${LightRed}Update!!!${NC}; fi)
#echo -e ${Red}ApacheDirectoryStudio${NC} ${Green}Installed:${NC} $CurrentApacheDirectoryStudio ${Green}Latest:${NC} $LatestApacheDirectoryStudio ${LightPurple}===${NC} $UpdateApacheDirectoryStudioCheck
CurrentAzureCLI=$(az version | grep 'azure-cli":' | awk -F '"' '{print $4}')
LatestAzureCLI=$(curl -s https://github.com/Azure/azure-cli/releases | grep 'releases/expanded_assets/' | awk -F 'azure-cli-' '{print $2}' | awk -F '"' '{print $1}' | sort -V | tail -1)
UpdateAzureCLICheck=$(if [ $CurrentAzureCLI == $LatestAzureCLI ]; then echo -e ${LightBlue}OK${NC}; else echo -e ${LightRed}Update!!!${NC}; fi)
echo -e ${Red}Azure-CLI${NC} ${Green}Installed:${NC} $CurrentAzureCLI ${Green}Latest:${NC} $LatestAzureCLI ${LightPurple}===${NC} $UpdateAzureCLICheck
CurrentCalibre=$(calibre --version | awk -F " " '{print $3}' | awk -F ")" '{print $1}')
LatestCalibre=$(curl -s https://calibre-ebook.com/download_linux | grep "latest release" | awk -F " is " '{print substr($2, 1, length($2)-2)}')
#LatestCalibre=$(curl -s https://calibre-ebook.com/download_linux | grep "latest release" | awk -F " is " '{print substr($2, 1, length($2)-4)}')
UpdateCalibreCheck=$(if [ $CurrentCalibre == $LatestCalibre ]; then echo -e ${LightBlue}OK${NC}; else echo -e ${LightRed}Update!!!${NC}; fi)
echo -e ${Red}Calibre${NC} ${Green}Installed:${NC} $CurrentCalibre ${Green}Latest:${NC} $LatestCalibre ${LightPurple}===${NC} $UpdateCalibreCheck
CurrentDisplayLink=$(ls /var/log/packages | grep displaylink | awk -F "-" '{print $2}' | awk -F ".0" '{print $1}')
LatestDisplayLink=$(curl -s https://www.synaptics.com/products/displaylink-graphics/downloads/ubuntu | grep release | awk -F "Ubuntu" 'NF >1 {print $2}' | head -2 | awk -F "-" 'NF >1 {print $1}')
UpdateDisplayLinkCheck=$(if [ $CurrentDisplayLink == $LatestDisplayLink ]; then echo -e ${LightBlue}OK${NC}; else echo -e ${LightRed}Update!!!${NC}; fi)
echo -e ${Red}DisplayLink${NC} ${Green}Installed:${NC} $CurrentDisplayLink ${Green}Latest:${NC} $LatestDisplayLink ${LightPurple}===${NC} $UpdateDisplayLinkCheck
CurrentDKMS=$(ls /var/log/packages | grep dkms | awk -F "-" '{print $2}')
LatestDKMS=$(git ls-remote --tags --refs --sort="version:refname" https://github.com/dell/dkms | awk -F/ 'END{print$NF}' | tr -d \v)
UpdateDKMSCheck=$(if [ $CurrentDKMS == $LatestDKMS ]; then echo -e ${LightBlue}OK${NC}; else echo -e ${LightRed}Update!!!${NC}; fi)
echo -e ${Red}DKMS${NC} ${Green}Installed:${NC} $CurrentDKMS ${Green}Latest:${NC} $LatestDKMS ${LightPurple}===${NC} $UpdateDKMSCheck
CurrentEVDI=$(ls /var/log/packages | grep evdi-1 | awk -F "-" '{print $2}')
LatestEVDI=$(git ls-remote --tags --refs --sort="version:refname" https://github.com/DisplayLink/evdi | awk -F/ 'END{print$NF}' | tr -d \v)
UpdateEVDICheck=$(if [ $CurrentEVDI == $LatestEVDI ]; then echo -e ${LightBlue}OK${NC}; else echo -e ${LightRed}Update!!!${NC}; fi)
echo -e ${Red}EVDI${NC} ${Green}Installed:${NC} $CurrentEVDI ${Green}Latest:${NC} $LatestEVDI ${LightPurple}===${NC} $UpdateEVDICheck
#CurrentFreeRDP=$(xfreerdp /version | awk '{print $5}')
#LatestFreeRDP=$(git ls-remote --tags --refs --sort="version:refname" https://github.com/FreeRDP/FreeRDP | awk -F/ 'END{print$NF}' | tr -d \v)
#UpdateFreeRDPCheck=$(if [ $CurrentFreeRDP == $LatestFreeRDP ]; then echo -e ${LightBlue}OK${NC}; else echo -e ${LightRed}Update!!!${NC}; fi)
#echo -e ${Red}FreeRDP${NC} ${Green}Installed:${NC} $CurrentFreeRDP ${Green}Latest:${NC} $LatestFreeRDP ${LightPurple}===${NC} $UpdateFreeRDPCheck
#CurrentGoogleChrome=$(ls /var/log/packages | grep google-chrome | awk -F "-" 'NF >3 {print $3}')
#LatestGoogleChrome=$(curl -s https://omahaproxy.appspot.com/all | grep "linux,stable" | awk -F "," 'NF >1 {print $3}')
#UpdateGoogleChromeCheck=$(if [ $CurrentGoogleChrome == $LatestGoogleChrome ]; then echo -e ${LightBlue}OK${NC}; else echo -e ${LightRed}Update!!!${NC}; fi)
#echo -e ${Red}GoogleChrome${NC} ${Green}Installed:${NC} $CurrentGoogleChrome ${Green}Latest:${NC} $LatestGoogleChrome ${LightPurple}===${NC} $UpdateGoogleChromeCheck
#CurrentFreeOffice=$(ls /var/log/packages | grep FreeOffice | awk -F "-" '{print $2}')
#LatestFreeOffice=$(curl -s https://www.freeoffice.com/en/download | grep ".deb" | tail -2 | head -1 | awk -F "-" 'NF > 1 {print $4}')
#UpdateFreeOfficeCheck=$(if [ $CurrentFreeOffice == $LatestFreeOffice ]; then echo -e ${LightBlue}OK${NC}; else echo -e ${LightRed}Update!!!${NC}; fi)
#echo -e ${Red}FreeOffice${NC} ${Green}Installed:${NC} $CurrentFreeOffice ${Green}Latest:${NC} $LatestFreeOffice ${LightPurple}===${NC} $UpdateFreeOfficeCheck
#CurrentKeePass=$(ls -ldh /usr/doc/KeePass* | awk -F "KeePass-" '{print $2}')
#LatestKeePass=$(curl -s https://sourceforge.net/projects/keepass/files/KeePass%202.x/ | grep latest | head -1 | awk -F "-" '{print $6}')
#UpdateKeePassCheck=$(if [ $CurrentKeePass == $LatestKeePass ]; then echo -e ${LightBlue}OK${NC}; else echo -e ${LightRed}Update!!!${NC}; fi)
#echo -e ${Red}KeePass${NC} ${Green}Installed:${NC} $CurrentKeePass ${Green}Latest:${NC} $LatestKeePass ${LightPurple}===${NC} $UpdateKeePassCheck
#CurrentLibDVDCSS=$(ls /var/log/packages | grep dvdcss | awk -F "-" '{print $2}')
#LatestLibDVDCSS=$(curl -s https://code.videolan.org/videolan/libdvdcss/-/tags | grep "Update NEWS" | head -1 | awk -F " for " '{print $2}' | awk -F "<" '{print $1}')
#UpdateLibDVDCSSCheck=$(if [ $CurrentLibDVDCSS == $LatestLibDVDCSS ]; then echo -e ${LightBlue}OK${NC}; else echo -e ${LightRed}Update!!!${NC}; fi)
#echo -e ${Red}LibDVDCSS${NC} ${Green}Installed:${NC} $CurrentLibDVDCSS ${Green}Latest:${NC} $LatestLibDVDCSS ${LightPurple}===${NC} $UpdateLibDVDCSSCheck
CurrentLibreOffice=$(ls /var/log/packages | grep libreoffice | awk -F "-" '{print $2}' | head -1 | tr -d \v)
LatestLibreOffice=$(curl -s https://slackware.nl/people/alien/slackbuilds/libreoffice/pkg64/current/ | awk -F "libreoffice-" 'NF >1 {print $2}' | head -1 | awk -F "-" 'NF >1 {print $1}')
UpdateLibreOfficeCheck=$(if [ $CurrentLibreOffice == $LatestLibreOffice ]; then echo -e ${LightBlue}OK${NC}; else echo -e ${LightRed}Update!!!${NC}; fi)
echo -e ${Red}LibreOffice${NC} ${Green}Installed:${NC} $CurrentLibreOffice ${Green}Latest:${NC} $LatestLibreOffice ${LightPurple}===${NC} $UpdateLibreOfficeCheck
CurrentMasterPDFEditor=$(ls /var/log/packages | grep MasterPDFEditor | awk -F "-" '{print $2}' | head -1 | tr -d \v)
LatestMasterPDFEditor=$(curl -s https://code-industry.net/free-pdf-editor/ | awk -F "master-pdf-editor" 'NF >1 {print $3}' | head -2 | awk -F "-" 'NF >1 {print $2}')
UpdateMasterPDFEditorCheck=$(if [ $CurrentMasterPDFEditor == $LatestMasterPDFEditor ]; then echo -e ${LightBlue}OK${NC}; else echo -e ${LightRed}Update!!!${NC}; fi)
echo -e ${Red}MasterPDFEditor${NC} ${Green}Installed:${NC} $CurrentMasterPDFEditor ${Green}Latest:${NC} $LatestMasterPDFEditor ${LightPurple}===${NC} $UpdateMasterPDFEditorCheck
#CurrentMetasploit=$(ls /var/log/packages | grep metasploit | awk -F "-" '{print $2}' | head -1 | tr -d \v)
#LatestMetasploit=$(curl -s https://github.com/rapid7/metasploit-framework/wiki/Downloads-by-Version | grep 'latest-linux-x64-installer.run"' | awk -F ">metasploit-" '{print $2}' | awk -F "-" '{print $1}')
#UpdateMetasploitCheck=$(if [ $CurrentMetasploit == $LatestMetasploit ]; then echo -e ${LightBlue}OK${NC}; else echo -e ${LightRed}Update!!!${NC}; fi)
#echo -e ${Red}Metasploit${NC} ${Green}Installed:${NC} $CurrentMetasploit ${Green}Latest:${NC} $LatestMetasploit ${LightPurple}===${NC} $UpdateMetasploitCheck
CurrentMicrosoftEdge=$(microsoft-edge-stable --version | awk -F " " 'NF >1 {print $3}')
#LatestMicrosoftEdge=$(curl -s https://packages.microsoft.com/yumrepos/edge/ | grep $(date +"%b-%Y") | tail -1 | awk -F "-" '{print $4}')
LatestMicrosoftEdge=$(curl -s https://packages.microsoft.com/yumrepos/edge/ | grep "stable" | grep $(date +"%Y") | awk -F "-" '{print $4}' | sort -nr | head -1)
UpdateMicrosoftEdgeCheck=$(if [ $CurrentMicrosoftEdge == $LatestMicrosoftEdge ]; then echo -e ${LightBlue}OK${NC}; else echo -e ${LightRed}Update!!!${NC}; fi)
echo -e ${Red}MicrosoftEdge${NC} ${Green}Installed:${NC} $CurrentMicrosoftEdge ${Green}Latest:${NC} $LatestMicrosoftEdge ${LightPurple}===${NC} $UpdateMicrosoftEdgeCheck
#CurrentMono=$(mono --version | head -1 | awk -F " " '{print $5}')
#LatestMono=$(curl -s https://download.mono-project.com/sources/mono/nightly/ | grep "mono-" | tail -1 | awk -F ".tar.xz" '{print $1}' | awk -F "mono-" '{print $2}')
#UpdateMonoCheck=$(if [ $CurrentMono == $LatestMono ]; then echo -e ${LightBlue}OK${NC}; else echo -e ${LightRed}Update!!!${NC}; fi)
#echo -e ${Red}Mono${NC} ${Green}Installed:${NC} $CurrentMono ${Green}Latest:${NC} $LatestMono ${LightPurple}===${NC} $UpdateMonoCheck
CurrentMuseScore=$(musescore --version | awk -F " " 'NF >1 {print $2}')
LatestMuseScore=$(curl -s https://musescore.org/en | grep '<span id="download-version">' | awk -F 'MuseScore ' 'NF >1 {print $2}' | awk -F '</span' 'NF >1 {print $1}')
UpdateMuseScoreCheck=$(if [ $CurrentMuseScore == $LatestMuseScore ]; then echo -e ${LightBlue}OK${NC}; else echo -e ${LightRed}Update!!!${NC}; fi)
echo -e ${Red}MuseScore${NC} ${Green}Installed:${NC} $CurrentMuseScore ${Green}Latest:${NC} $LatestMuseScore ${LightPurple}===${NC} $UpdateMuseScoreCheck
CurrentNVIDIA=$(grep "X Driver" /var/log/Xorg.0.log | awk '{print $8}')
LatestNVIDIA=$(git ls-remote --tags --refs --sort="version:refname" https://github.com/NVIDIA/nvidia-xconfig | awk -F/ 'END{print$NF}' | tr -d \v)
UpdateNVIDIACheck=$(if [ $CurrentNVIDIA == $LatestNVIDIA ]; then echo -e ${LightBlue}OK${NC}; else echo -e ${LightRed}Update!!!${NC}; fi)
echo -e ${Red}NVIDIA${NC} ${Green}Installed:${NC} $CurrentNVIDIA ${Green}Latest:${NC} $LatestNVIDIA ${LightPurple}===${NC} $UpdateNVIDIACheck
CurrentOpenConnect=$(openconnect --version | grep "OpenConnect" | awk -F "version v" '{print $2}')
LatestOpenConnect=$(curl -ks https://www.infradead.org/openconnect/download/ | tail -5 | head -1 | awk -F "openconnect-" '{print $2}' | awk -F ".tar" '{print $1}')
UpdateOpenConnectCheck=$(if [ $CurrentOpenConnect == $LatestOpenConnect ]; then echo -e ${LightBlue}OK${NC}; else echo -e ${LightRed}Update!!!${NC}; fi)
echo -e ${Red}OpenConnect${NC} ${Green}Installed:${NC} $CurrentOpenConnect ${Green}Latest:${NC} $LatestOpenConnect ${LightPurple}===${NC} $UpdateOpenConnectCheck
CurrentOpenConnectSSO=$(openconnect-sso --version | awk -F " " '{print $2}')
LatestOpenConnectSSO=$(git ls-remote --tags --refs --sort="version:refname" https://github.com/vlaci/openconnect-sso | awk -F/ 'END{print$NF}' | tr -d \v)
UpdateOpenConnectSSOCheck=$(if [ $CurrentOpenConnectSSO == $LatestOpenConnectSSO ]; then echo -e ${LightBlue}OK${NC}; else echo -e ${LightRed}Update!!!${NC}; fi)
echo -e ${Red}OpenConnect-SSO${NC} ${Green}Installed:${NC} $CurrentOpenConnectSSO ${Green}Latest:${NC} $LatestOpenConnectSSO ${LightPurple}===${NC} $UpdateOpenConnectSSOCheck
CurrentPowerShell=$(pwsh --version | awk -F ' ' '{print $2}')
LatestPowerShell=$(git ls-remote --tags --refs --sort="version:refname" https://github.com/PowerShell/PowerShell.git | awk -F/ 'END{print$NF}' | tr -d \v)
UpdatePowerShellCheck=$(if [ $CurrentPowerShell == $LatestPowerShell ]; then echo -e ${LightBlue}OK${NC}; else echo -e ${LightRed}Update!!!${NC}; fi)
echo -e ${Red}PowerShell${NC} ${Green}Installed:${NC} $CurrentPowerShell ${Green}Latest:${NC} $LatestPowerShell ${LightPurple}===${NC} $UpdatePowerShellCheck
#CurrentQalculate=$(qalculate-gtk --version)
#LatestQalculate=$(git ls-remote --tags --refs --sort="version:refname" https://github.com/Qalculate/qalculate-gtk.git | awk -F/ 'END{print$NF}' | tr -d \v)
#UpdateQalculateCheck=$(if [ $CurrentQalculate == $LatestQalculate ]; then echo -e ${LightBlue}OK${NC}; else echo -e ${LightRed}Update!!!${NC}; fi)
#echo -e ${Red}Qalculate${NC} ${Green}Installed:${NC} $CurrentQalculate ${Green}Latest:${NC} $LatestQalculate ${LightPurple}===${NC} $UpdateQalculateCheck
CurrentRemmina=$(ls /var/log/packages | grep Remmina | awk -F "-" '{print $2}' | tr -d \v)
LatestRemmina=$(git ls-remote --tags --refs --sort="version:refname" https://gitlab.com/Remmina/Remmina.git | awk -F/ 'END{print$NF}' | tr -d \v)
UpdateRemminaCheck=$(if [ $CurrentRemmina == $LatestRemmina ]; then echo -e ${LightBlue}OK${NC}; else echo -e ${LightRed}Update!!!${NC}; fi)
echo -e ${Red}Remmina${NC} ${Green}Installed:${NC} $CurrentRemmina ${Green}Latest:${NC} $LatestRemmina ${LightPurple}===${NC} $UpdateRemminaCheck
CurrentSignal=$(ls /var/log/packages | grep signal | awk -F "-" '{print $3}')
LatestSignal=$(curl -s https://github.com/signalapp/Signal-Desktop/tags | grep "releases/tag/" | head -1 | awk -F "/" '{print $6}' | awk -F '"' '{print $1}' | awk -F "v" '{print $2}')
UpdateSignalCheck=$(if [ $CurrentSignal == $LatestSignal ]; then echo -e ${LightBlue}OK${NC}; else echo -e ${LightRed}Update!!!${NC}; fi)
echo -e ${Red}Signal${NC} ${Green}Installed:${NC} $CurrentSignal ${Green}Latest:${NC} $LatestSignal ${LightPurple}===${NC} $UpdateSignalCheck
#CurrentSlack=$(slack --version)
#LatestSlack=4.28.171
#LatestSlack=$(curl -s https://slack.com/downloads/mac | awk -F "Version " 'NF >1 {print $2}' | awk -F "</span>" 'NF >1 {print $1}')
#UpdateSlackCheck=$(if [ $CurrentSlack == $LatestSlack ]; then echo -e ${LightBlue}OK${NC}; else echo -e ${LightRed}Update!!!${NC}; fi)
#echo -e ${Red}Slack${NC} ${Green}Installed:${NC} $CurrentSlack ${Green}Latest:${NC} $LatestSlack ${LightPurple}===${NC} $UpdateSlackCheck
CurrentSolaar=$(solaar --version | awk -F " " '{print $2}')
LatestSolaar=$(curl -s https://github.com/pwr-Solaar/Solaar/tags | grep "releases/tag/" | head -1 | awk -F "/" '{print $6}' | awk -F '"' '{print $1}')
UpdateSolaarCheck=$(if [ $CurrentSolaar == $LatestSolaar ]; then echo -e ${LightBlue}OK${NC}; else echo -e ${LightRed}Update!!!${NC}; fi)
echo -e ${Red}Solaar${NC} ${Green}Installed:${NC} $CurrentSolaar ${Green}Latest:${NC} $LatestSolaar ${LightPurple}===${NC} $UpdateSolaarCheck
CurrentTeams=$(ls /var/log/packages | grep teams | awk -F "-" '{print $2}' | tr -d \v)
LatestTeams=$(curl -s https://packages.microsoft.com/repos/ms-teams/pool/main/t/teams/ | awk -F '_' '{print $2}' | sort -V | tail -1)
InsidersTeams=$(curl -s https://packages.microsoft.com/repos/ms-teams/pool/main/t/teams-insiders/ | awk -F '_' '{print $2}' | sort -V | tail -1)
UpdateTeamsCheck=$(if [ $CurrentTeams == $LatestTeams ]; then echo -e ${LightBlue}OK${NC}; else echo -e ${LightRed}Update!!!${NC}; fi)
echo -e ${Red}Teams${NC} ${Green}Installed:${NC} $CurrentTeams ${Green}Latest:${NC} $LatestTeams ${Green}Insiders:${NC} $InsidersTeams ${LightPurple}===${NC} $UpdateTeamsCheck
CurrentTeamsAppImage=$(ls /Documents/Fernando/Negocio/Chambas/HCCS/MSTeams/ | awk -F ".AppImage" '{print $1}' | awk -F "linux-" '{print $2}')
LatestTeamsAppImage=$(git ls-remote --tags --refs --sort="version:refname" https://github.com/IsmaelMartinez/teams-for-linux | awk -F/ 'END{print$NF}' | tr -d \v)
UpdateTeamsAppImageCheck=$(if [ $CurrentTeamsAppImage == $LatestTeamsAppImage ]; then echo -e ${LightBlue}OK${NC}; else echo -e ${LightRed}Update!!!${NC}; fi)
echo -e ${Red}TeamsAppImage${NC} ${Green}Installed:${NC} $CurrentTeamsAppImage ${Green}Latest:${NC} $LatestTeamsAppImage ${LightPurple}===${NC} $UpdateTeamsAppImageCheck
CurrentVirtualBox=$(vboxmanage --version | awk -F "r" 'NF >1 {print $1}')
#LatestVirtualBox=$(curl -s https://www.virtualbox.org/wiki/Downloads | grep "<h3 id=\"VirtualBox" | head -1 | awk -F " " 'NF >1 {print $5}')
LatestVirtualBox=$(curl -s http://download.virtualbox.org/virtualbox/LATEST.TXT)
TestBuildsVirtualBox=$(curl -s https://www.virtualbox.org/wiki/Testbuilds | grep "Linux_amd64" | head -1 | awk -F "-" '{print $2}')
UpdateVirtualBoxCheck=$(if [ $CurrentVirtualBox == $LatestVirtualBox ]; then echo -e ${LightBlue}OK${NC}; else echo -e ${LightRed}Update!!!${NC}; fi)
echo -e ${Red}VirtualBox${NC} ${Green}Installed:${NC} $CurrentVirtualBox ${Green}Latest:${NC} $LatestVirtualBox ${Green}TestBuilds:${NC} $TestBuildsVirtualBox ${LightPurple}===${NC} $UpdateVirtualBoxCheck
#CurrentVisualStudioCode=$(code --version | head -1)
#LatestVisualStudioCode=$(curl -s https://github.com/microsoft/vscode/releases | grep '<a href="/microsoft/vscode/releases/tag/' | awk -F 'tag/' '{print $2}' | awk -F '"' '{print $1}' | sort -V | tail -1)
#UpdateVisualStudioCodeCheck=$(if [ $CurrentVisualStudioCode == $LatestVisualStudioCode ]; then echo -e ${LightBlue}OK${NC}; else echo -e ${LightRed}Update!!!${NC}; fi)
#echo -e ${Red}VisualStudioCode${NC} ${Green}Installed:${NC} $CurrentVisualStudioCode ${Green}Latest:${NC} $LatestVisualStudioCode ${LightPurple}===${NC} $UpdateVisualStudioCodeCheck
CurrentVSCodium=$(codium --version | head -1)
#LatestVSCodium=1.71.0
LatestVSCodium=$(curl -s https://github.com/VSCodium/vscodium/releases | grep '/VSCodium/vscodium/releases/download/' | awk -F 'download/' '{print $2}' | awk -F '/' '{print substr($1, 1, length($1)-6)}' | sort -V | tail -1)
UpdateVSCodiumCheck=$(if [ $CurrentVSCodium == $LatestVSCodium ]; then echo -e ${LightBlue}OK${NC}; else echo -e ${LightRed}Update!!!${NC}; fi)
echo -e ${Red}VSCodium${NC} ${Green}Installed:${NC} $CurrentVSCodium ${Green}Latest:${NC} $LatestVSCodium ${LightPurple}===${NC} $UpdateVSCodiumCheck
#CurrentWhatWeb=$(whatweb --version | awk -F " " '{print $3}')
#LatestWhatWeb=$(git ls-remote --tags --refs --sort="version:refname" https://github.com/urbanadventurer/WhatWeb | awk -F/ 'END{print$NF}' | tr -d \v)
#UpdateWhatWebCheck=$(if [ $CurrentWhatWeb == $LatestWhatWeb ]; then echo -e ${LightBlue}OK${NC}; else echo -e ${LightRed}Update!!!${NC}; fi)
#echo -e ${Red}WhatWeb${NC} ${Green}Installed:${NC} $CurrentWhatWeb ${Green}Latest:${NC} $LatestWhatWeb ${LightPurple}===${NC} $UpdateWhatWebCheck
#CurrentWireShark=$(wireshark --version | grep "Wireshark" | awk -F " " 'NF >1 {print $2}')
#LatestWireShark=$(curl -s https://www.wireshark.org/download/src/ | grep "wireshark" | tail -2 | awk -F "wireshark-" 'NF >1 {print $2}' | head -1 | awk -F ".tar.xz" 'NF >1 {print $1}')
#UpdateWireSharkCheck=$(if [ $CurrentWireShark == $LatestWireShark ]; then echo -e ${LightBlue}OK${NC}; else echo -e ${LightRed}Update!!!${NC}; fi)
#echo -e ${Red}Wireshark${NC} ${Green}Installed:${NC} $CurrentWireShark ${Green}Latest:${NC} $LatestWireShark ${LightPurple}===${NC} $UpdateWireSharkCheck
CurrentXournalpp=$(xournalpp --version | grep Xournal | awk -F " " 'NF >1 {print $2}')
LatestXournalpp=$(git ls-remote --tags --refs --sort="version:refname" https://github.com/xournalpp/xournalpp | awk -F/ 'END{print$NF}' | tr -d \v)
UpdateXournalppCheck=$(if [ $CurrentXournalpp == $LatestXournalpp ]; then echo -e ${LightBlue}OK${NC}; else echo -e ${LightRed}Update!!!${NC}; fi)
echo -e ${Red}Xournal++${NC} ${Green}Installed:${NC} $CurrentXournalpp ${Green}Latest:${NC} $LatestXournalpp ${LightPurple}===${NC} $UpdateXournalppCheck
#CurrentYouTubeDL=$(youtube-dl --version)
#LatestYouTubeDL=$(git ls-remote --tags --refs --sort="version:refname" https://github.com/ytdl-org/youtube-dl | awk -F/ 'END{print$NF}' | tr -d \v)
#UpdateYouTubeDLCheck=$(if [ $CurrentYouTubeDL == $LatestYouTubeDL ]; then echo -e ${LightBlue}OK${NC}; else echo -e ${LightRed}Update!!!${NC}; fi)
#echo -e ${Red}YouTubeDL${NC} ${Green}Installed:${NC} $CurrentYouTubeDL ${Green}Latest:${NC} $LatestYouTubeDL ${LightPurple}===${NC} $UpdateYouTubeDLCheck
CurrentZoom=$(ls /var/log/packages | grep zoom | awk -F "-" '{print $3}')
LatestZoom=$(curl -s https://zoom.us/support/download --header 'User-Agent: Mozilla/5.0 (X11; Linux x86_64)' | grep "class=\"linux-ver-text\"" | awk -F "Version " 'NF >1 {print $2}' | awk -F "<" 'NF >1 {print substr($1, 1, length($1)-1)}' | sed "s/ (/./g")
UpdateZoomCheck=$(if [ $CurrentZoom == $LatestZoom ]; then echo -e ${LightBlue}OK${NC}; else echo -e ${LightRed}Update!!!${NC}; fi)
echo -e ${Red}Zoom${NC} ${Green}Installed:${NC} $CurrentZoom ${Green}Latest:${NC} $LatestZoom ${LightPurple}===${NC} $UpdateZoomCheck
