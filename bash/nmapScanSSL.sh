#!/bin/bash
declare -a StringArray=("<Subnet1>/<SlashNotation1>" "<Subnet2>/<SlashNotation2>" "<Subnet3>/<SlashNotation3>")

for val in "${StringArray[@]}"; do
  nmapvar=$(echo $val | tr --delete /);
  nmap -sV -sC -vv $val -oX SSLScan/$nmapvar.xml
done
