Export VM list + location KVM
for n in $(virsh list --name) ; do outlosf=$(lsof | grep 'qcow2\|-sd' | grep $n | head -1 | awk -F " " '{print $9}') ; echo $n $outlosf ; done | sort | column -t

Check VM migration progress
ImageName=<imagename>.qcow2
echo $ImageName Source: $(ll /var/lib/libvirt/images/$ImageName | awk -F " " '{print $5}') Target: $(ll /var/lib/libvirt/flash-images/$ImageName | awk -F " " '{print $5}')
echo $ImageName Source: $(ls -lah /var/lib/libvirt/images/$ImageName | awk -F " " '{print $5}') Target: $(ls -lah /var/lib/libvirt/flash-images/$ImageName | awk -F " " '{print $5}')

Change /var/lib/libvirt/images/ to source
Change /var/lib/libvirt/flash-images/ to target
