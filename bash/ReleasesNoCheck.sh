#!/bin/bash
#Color Codes
Black='\033[0;30m'
Blue='\033[0;34m'
BrownOrange='\033[0;33m'
Cyan='\033[0;36m'
DarkGray='\033[1;30m'
Green='\033[0;32m'
LightBlue='\033[1;34m'
LightCyan='\033[1;36m'
LightGray='\033[0;37m'
LightGreen='\033[1;32m'
LightMagenta='\033[1;35m'
LightPurple='\033[1;35m'
LightRed='\033[1;31m'
LightYellow='\033[1;33m'
Magenta='\033[0;35m'
NC='\033[0m' # No Color
Purple='\033[0;35m'
Red='\033[0;31m'
White='\033[1;37m'
Yellow='\033[0;33m'

CurrentDate=$(date +"%Y-%m-%d %T")
echo -e ${Red}Date${NC} ${Green}Current Date + Time:${NC} $CurrentDate
InstalledKernel=$(uname -r)
LatestKernel=$(curl -s https://www.kernel.org/finger_banner | grep "latest stable version" | awk -F ":" 'NF >1 {print $2}')
echo -e ${Red}Kernel${NC} ${Green}Installed:${NC} $InstalledKernel ${Green}Latest:${NC} $LatestKernel
InstalledSlackware64Current=$(find /var/log/packages/ -printf '%T+ %p\n' | sort -r | head -1 | awk -F "+" 'NF >1 {print $1}')
LatestSlackware64Current=$(curl -s https://ftp.osuosl.org/pub/slackware/slackware64-current/ | grep ChangeLog | awk -F ">" 'NF >1 {print $10}' | awk -F " " 'NF >1 {print $1}')
echo -e ${Red}Slackware64-current${NC} ${Green}Installed:${NC} $InstalledSlackware64Current ${Green}Latest:${NC} $LatestSlackware64Current
echo ""
Current1Password=$(1password --version)
Latest1Password=$(curl -s https://releases.1password.com/linux/ | grep -m 1 "Updated to" | awk -F "Updated to " '{print $2}' | awk -F " on" '{print $1}')
echo -e ${Red}1Password${NC} ${Green}Current:${NC} $Current1Password ${Green}Latest:${NC} $Latest1Password
CurrentAngryIPScanner=$(ls /var/log/packages | grep ipscan | awk -F "-" '{print $2}')
LatestAngryIPScanner=$(git ls-remote --tags --refs --sort="version:refname" https://github.com/angryip/ipscan | awk -F/ 'END{print$NF}' | tr -d \v)
echo -e ${Red}AngryIPScanner${NC} ${Green}Current:${NC} $CurrentAngryIPScanner ${Green}Latest:${NC} $LatestAngryIPScanner
CurrentDisplayLink=$(ls /var/log/packages | grep displaylink | awk -F "-" '{print $2}')
LatestDisplayLink=$(curl -s https://www.synaptics.com/products/displaylink-graphics/downloads/ubuntu | grep release | awk -F "Ubuntu" 'NF >1 {print $2}' | head -2 | awk -F "-" 'NF >1 {print $1}')
echo -e ${Red}DisplayLink${NC} ${Green}Current:${NC} $CurrentDisplayLink ${Green}Latest:${NC} $LatestDisplayLink
CurrentDKMS=$(ls /var/log/packages | grep dkms | awk -F "-" '{print $2}')
LatestDKMS=$(git ls-remote --tags --refs --sort="version:refname" https://github.com/dell/dkms | awk -F/ 'END{print$NF}' | tr -d \v)
echo -e ${Red}DKMS${NC} ${Green}Current:${NC} $CurrentDKMS ${Green}Latest:${NC} $LatestDKMS
CurrentEVDI=$(ls /var/log/packages | grep evdi-1 | awk -F "-" '{print $2}')
LatestEVDI=$(git ls-remote --tags --refs --sort="version:refname" https://github.com/DisplayLink/evdi | awk -F/ 'END{print$NF}' | tr -d \v)
echo -e ${Red}EVDI${NC} ${Green}Current:${NC} $CurrentEVDI ${Green}Latest:${NC} $LatestEVDI
#CurrentFreeRDP=$(xfreerdp /version | awk '{print $5}')
#LatestFreeRDP=$(git ls-remote --tags --refs --sort="version:refname" https://github.com/FreeRDP/FreeRDP | awk -F/ 'END{print$NF}' | tr -d \v)
#echo -e ${Red}FreeRDP${NC} ${Green}Current:${NC} $CurrentFreeRDP ${Green}Latest:${NC} $LatestFreeRDP
#CurrentGoogleChrome=$(ls /var/log/packages | grep google-chrome | awk -F "-" 'NF >3 {print $3}')
#LatestGoogleChrome=$(curl -s https://omahaproxy.appspot.com/all | grep "linux,stable" | awk -F "," 'NF >1 {print $3}')
#echo -e ${Red}GoogleChrome${NC} ${Green}Current:${NC} $CurrentGoogleChrome ${Green}Latest:${NC} $LatestGoogleChrome
CurrentFreeOffice=$(ls /var/log/packages | grep FreeOffice | awk -F "-" '{print $2}')
LatestFreeOffice=$(curl -s https://www.freeoffice.com/en/download | grep ".deb" | tail -2 | head -1 | awk -F "-" 'NF > 1 {print $4}')
echo -e ${Red}FreeOffice${NC} ${Green}Current:${NC} $CurrentFreeOffice ${Green}Latest:${NC} $LatestFreeOffice
CurrentLibreOffice=$(ls /var/log/packages | grep libreoffice | awk -F "-" '{print $2}' | head -1 | tr -d \v)
LatestLibreOffice=$(curl -s https://slackware.nl/people/alien/slackbuilds/libreoffice/pkg64/current/ | awk -F "libreoffice-" 'NF >1 {print $2}' | head -1 | awk -F "-" 'NF >1 {print $1}')
echo -e ${Red}LibreOffice${NC} ${Green}Current:${NC} $CurrentLibreOffice ${Green}Latest:${NC} $LatestLibreOffice
CurrentMasterPDFEditor=$(ls /var/log/packages | grep MasterPDFEditor | awk -F "-" '{print $2}' | head -1 | tr -d \v)
LatestMasterPDFEditor=$(curl -s https://code-industry.net/free-pdf-editor/ | awk -F "master-pdf-editor" 'NF >1 {print $3}' | head -2 | awk -F "-" 'NF >1 {print $2}')
echo -e ${Red}MasterPDFEditor${NC} ${Green}Current:${NC} $CurrentMasterPDFEditor ${Green}Latest:${NC} $LatestMasterPDFEditor
CurrentMicrosoftEdge=$(cd /opt/microsoft/msedge && microsoft-edge-stable --version | awk -F " " 'NF >1 {print $3}')
LatestMicrosoftEdge=$(curl -s https://packages.microsoft.com/repos/edge/pool/main/m/microsoft-edge-stable/ | tail -3 | head -1 | awk -F "_" 'NF >1 {print $2}' | awk -F "-" 'NF >1 {print $1}')
echo -e ${Red}MicrosoftEdge${NC} ${Green}Current:${NC} $CurrentMicrosoftEdge ${Green}Latest:${NC} $LatestMicrosoftEdge
CurrentMuseScore=$(musescore --version | awk -F " " 'NF >1 {print $2}')
LatestMuseScore=$(curl -s https://musescore.org/en | grep '<span id="download-version">' | awk -F 'MuseScore ' 'NF >1 {print $2}' | awk -F '</span' 'NF >1 {print $1}')
echo -e ${Red}MuseScore${NC} ${Green}Current:${NC} $CurrentMuseScore ${Green}Latest:${NC} $LatestMuseScore
CurrentNVIDIA=$(grep "X Driver" /var/log/Xorg.0.log | awk '{print $8}')
LatestNVIDIA=$(git ls-remote --tags --refs --sort="version:refname" https://github.com/NVIDIA/nvidia-xconfig | awk -F/ 'END{print$NF}' | tr -d \v)
echo -e ${Red}NVIDIA${NC} ${Green}Current:${NC} $CurrentNVIDIA ${Green}Latest:${NC} $LatestNVIDIA
CurrentOpenConnect=$(openconnect --version | grep "OpenConnect" | awk -F "version v" '{print $2}')
LatestOpenConnect=$(curl -s https://www.infradead.org/openconnect/download/ | tail -5 | head -1 | awk -F "openconnect-" '{print $2}' | awk -F ".tar" '{print $1}')
echo -e ${Red}OpenConnect${NC} ${Green}Current:${NC} $CurrentOpenConnect ${Green}Latest:${NC} $LatestOpenConnect
CurrentOpenConnectSSO=$(openconnect-sso --version | awk -F " " '{print $2}')
LatestOpenConnectSSO=$(git ls-remote --tags --refs --sort="version:refname" https://github.com/vlaci/openconnect-sso | awk -F/ 'END{print$NF}' | tr -d \v)
echo -e ${Red}OpenConnect-SSO${NC} ${Green}Current:${NC} $CurrentOpenConnectSSO ${Green}Latest:${NC} $LatestOpenConnectSSO
CurrentRemmina=$(ls /var/log/packages | grep Remmina | awk -F "-" '{print $2}' | tr -d \v)
LatestRemmina=$(git ls-remote --tags --refs --sort="version:refname" https://gitlab.com/Remmina/Remmina.git | awk -F/ 'END{print$NF}' | tr -d \v)
echo -e ${Red}Remmina${NC} ${Green}Current:${NC} $CurrentRemmina ${Green}Latest:${NC} $LatestRemmina
CurrentSlack=$(slack --version)
LatestSlack=$(curl -s https://slack.com/downloads/linux | awk -F "Version " 'NF >1 {print $2}' | awk -F "</span>" 'NF >1 {print $1}')
echo -e ${Red}Slack${NC} ${Green}Current:${NC} $CurrentSlack ${Green}Latest:${NC} $LatestSlack
CurrentTeams=$(ls /var/log/packages | grep teams | awk -F "-" '{print $2}' | tr -d \v)
LatestTeams=$(curl -s https://packages.microsoft.com/repos/ms-teams/pool/main/t/teams/ | tail -5 | awk -F "_" 'NF >1 {print $2}' | head -1)
InsidersTeams=$(curl -s https://packages.microsoft.com/repos/ms-teams/pool/main/t/teams-insiders/ | tail -3 | awk -F "_" 'NF >1 {print $2}' | head -1)
#LatestTeams=$(curl -s https://packages.microsoft.com/repos/ms-teams/pool/main/t/teams/ | grep $(date +"%Y") | head -2 | awk -F "_" 'NF >1 {print $2}' | tail -1)
#InsidersTeams=$(curl -s https://packages.microsoft.com/repos/ms-teams/pool/main/t/teams-insiders/ | grep $(date +"%Y") | head -2 | awk -F "_" 'NF >1 {print $2}' | tail -1)
echo -e ${Red}Teams${NC} ${Green}Current:${NC} $CurrentTeams ${Green}Latest:${NC} $LatestTeams ${Green}Insiders:${NC} $InsidersTeams
CurrentVirtualBox=$(vboxmanage --version | awk -F "r" 'NF >1 {print $1}')
LatestVirtualBox=$(curl -s https://www.virtualbox.org/wiki/Downloads | grep "<h3 id=\"VirtualBox" | head -1 | awk -F " " 'NF >1 {print $5}')
echo -e ${Red}VirtualBox${NC} ${Green}Current:${NC} $CurrentVirtualBox ${Green}Latest:${NC} $LatestVirtualBox
#CurrentHorizon=$(cat Horizon.txt)
#echo -e ${Red}VMwareHorizon${NC} ${Green}Current:${NC} $CurrentHorizon #${Green}Latest:${NC} $LatestVirtualBox
CurrentWireShark=$(wireshark --version | grep "Wireshark" | awk -F " " 'NF >1 {print $2}')
LatestWireShark=$(curl -s https://www.wireshark.org/download/src/ | grep "wireshark" | tail -2 | awk -F "wireshark-" 'NF >1 {print $2}' | head -1 | awk -F ".tar.xz" 'NF >1 {print $1}')
echo -e ${Red}Wireshark${NC} ${Green}Current:${NC} $CurrentWireShark ${Green}Latest:${NC} $LatestWireShark
CurrentXournalApp=$(xournalpp --version | grep Xournal | awk -F " " 'NF >1 {print $2}')
LatestXournalApp=$(git ls-remote --tags --refs --sort="version:refname" https://github.com/xournalpp/xournalpp | awk -F/ 'END{print$NF}' | tr -d \v)
echo -e ${Red}Xournal++${NC} ${Green}Current:${NC} $CurrentXournalApp ${Green}Latest:${NC} $LatestXournalApp
CurrentYouTubeDL=$(youtube-dl --version)
LatestYouTubeDL=$(git ls-remote --tags --refs --sort="version:refname" https://github.com/ytdl-org/youtube-dl | awk -F/ 'END{print$NF}' | tr -d \v)
echo -e ${Red}YouTubeDL${NC} ${Green}Current:${NC} $CurrentYouTubeDL ${Green}Latest:${NC} $LatestYouTubeDL
CurrentZoom=$(ls /var/log/packages | grep zoom | awk -F "-" '{print $3}')
LatestZoom=$(curl -s https://zoom.us/support/download --header 'User-Agent: Mozilla/5.0 (X11; Linux x86_64)' | grep "class=\"linux-ver-text\"" | awk -F "Version " 'NF >1 {print $2}' | awk -F "<" 'NF >1 {print $1}')
echo -e ${Red}Zoom${NC} ${Green}Current:${NC} $CurrentZoom ${Green}Latest:${NC} $LatestZoom
