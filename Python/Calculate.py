#!/usr/bin/python

#10-21-2020:  Script creation
#01-07-2021:  Changed {:.2f} to {:,.2f} to include thousand and million separators and added print("") to "Outputs" section for easier reading 
#06-18-2022:  Added check for empty entries for department usage to be replaced with 0

#Each section
Dep1 = "Department1 Lease (Model 1): "
Dep1Base = 296.08
Dep1Fax = 21.1
Dep1Fax2Dec = "{:,.2f}".format(Dep1Fax)
Dep1Usage = raw_input("Enter Department1 (Model 1) Usage: $")
if Dep1Usage == '':
  Dep1Usage = 0
Dep1UsageNum = float(Dep1Usage)
Dep1Taxes = (Dep1Base+Dep1Usage+Dep1Fax)*0.0825
Dep1Taxes2Dec = str(round(Dep1Taxes,2))
Dep1TaxesFin = float(Dep1Taxes2Dec)
Dep1Summed = Dep1Base + Dep1Fax + Dep1UsageNum + Dep1TaxesFin

Dep2 = "Department2 Lease (Model 2): "
Dep2Base = 170.93
Dep2Usage = raw_input("Enter Department2 (Model 2) Usage: $")
if Dep2Usage == '':
  Dep2Usage = 0
Dep2UsageNum = float(Dep2Usage)
Dep2Taxes = (Dep2Base+Dep2Usage)*0.0825
Dep2Taxes2Dec = str(round(Dep2Taxes,2))
Dep2TaxesFin = float(Dep2Taxes2Dec)
Dep2Summed = Dep2Base + Dep2UsageNum + Dep2TaxesFin

Dep3 = "Department3 Lease (Model 3): "
Dep3Base = 179.35
Dep3Usage = raw_input("Enter Department3 (Model 3) Usage: $")
if Dep3Usage == '':
  Dep3Usage = 0
Dep3UsageNum = float(Dep3Usage)
Dep3Taxes = (Dep3Base+Dep3Usage)*0.0825
Dep3Taxes2Dec = str(round(Dep3Taxes,2))
Dep3TaxesFin = float(Dep3Taxes2Dec)
Dep3Summed = Dep3Base + Dep3UsageNum + Dep3TaxesFin

Dep4 = "Department4 Lease (Model 4): "
Dep4Base = 353.19
Dep4Usage = raw_input("Enter Department4 (Model 4) Usage: $")
if Dep4Usage == '':
  Dep4Usage = 0
Dep4UsageNum = float(Dep4Usage)
Dep4Taxes = (Dep4Base+Dep4Usage)*0.0825
Dep4Taxes2Dec = str(round(Dep4Taxes,2))
Dep4TaxesFin = float(Dep4Taxes2Dec)
Dep4Summed = Dep4Base + Dep4UsageNum + Dep4TaxesFin 


#Putting everything together
Dep1Total = Dep1 + "$" + "{:,.2f}".format(Dep1Base) + " + $" + Dep1Fax2Dec + " + $" + "{:,.2f}".format(Dep1Usage) + " + $" + "{:,.2f}".format(Dep1TaxesFin) + " (Tax) = $" + "{:,.2f}".format(Dep1Summed)
Dep2Total = Dep2 + "$" + "{:,.2f}".format(Dep2Base) + " + $" + "{:,.2f}".format(Dep2Usage) + " + $" + "{:,.2f}".format(Dep2TaxesFin) + " (Tax) = $" + "{:,.2f}".format(Dep2Summed)
Dep3Total = Dep3 + "$" + "{:,.2f}".format(Dep3Base) + " + $" + "{:,.2f}".format(Dep3Usage) + " + $" + "{:,.2f}".format(Dep3TaxesFin) + " (Tax) = $" + "{:,.2f}".format(Dep3Summed)
Dep4Total = Dep4 + "$" + "{:,.2f}".format(Dep4Base) + " + $" + "{:,.2f}".format(Dep4Usage) + " + $" + "{:,.2f}".format(Dep4TaxesFin) + " (Tax) = $" + "{:,.2f}".format(Dep4Summed)
GrandTotal = GrandTotal = "Grand Total: $" + "{:,.2f}".format(Dep1Summed + Dep2Summed + Dep3Summed + Dep4Summed)

# Outputs
print("")
print(Dep1Total)
print(Dep2Total)
print(Dep3Total)
print(Dep4Total)
print("")
print(GrandTotal)
